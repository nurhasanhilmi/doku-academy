import java.util.Scanner;

public class LampButton {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number = scanner.nextInt();

        if (isTurnedOn(number)) {
            System.out.println("lampu menyala");
        } else {
            System.out.println("lampu mati");
        }
    }

    private static boolean isTurnedOn(int N) {
        boolean turnedOn = false;
        for (int i = 1; i <= N; i++)
            if (N % i == 0)
                turnedOn = !turnedOn;

        return turnedOn;
    }
}
