#!/bin/sh

name=$1
fb_name=$2
linkedin_name=$3

main_dir="$name at $(date)"

mkdir "$main_dir"
mkdir "$main_dir"/about_me
mkdir "$main_dir"/about_me/personal
mkdir "$main_dir"/about_me/professional
mkdir "$main_dir"/my_friends "$main_dir"/my_system_info

echo https://www.facebook.com/$fb_name > "$main_dir"/about_me/personal/facebook.txt
echo https://www.linkedin.com/in/$linkedin_name > "$main_dir"/about_me/professional/linkedin.txt

url=https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%2520of%2520my%2520friends.txt
curl -s $url >> "$main_dir"/my_friends/list_of_my_friends.txt

echo "My username: $(whoami)" > "$main_dir"/my_system_info/about_this_laptop.txt
echo $(uname -a) >> "$main_dir"/my_system_info/about_this_laptop.txt

echo "Connection to google:" > "$main_dir"/my_system_info/internet_connection.txt
ping -c 3 forcesafesearch.google.com >> "$main_dir"/my_system_info/internet_connection.txt
